package com.ui.automation.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Eyasu
 *
 */
public class DBUtil {
	private static String URL = ConfigManager.getProperty("URLAzure");
	private static String User = ConfigManager.getProperty("UserAzure");
	private static String Password = ConfigManager.getProperty("PasswordAzure");
	private static Statement statement = null;
	private static ResultSet rs = null;
	// private static String pullQuery = "select * from ISERVE.Applicant";
	String Environment = "Test";
	String URLToTest = "UserLogin";

	public static Connection getConnection() throws Exception {
		Connection connect = null;
		try {
			Class.forName(ConfigManager.getProperty("DBDriver"));
			connect = DriverManager.getConnection(URL, User, Password);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return connect;
	}

	public static Statement getDBStatement() throws Exception {
		try {
			statement = getConnection().createStatement();
//			System.out.println(URL + "Connected");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return statement;
	}

	public static ResultSet getResultset() throws Exception {
		try {
			getDBStatement();
			// rs = statement.executeQuery(pullQuery);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rs;
	}

	public static void main(String[] args) throws Exception {
		DBUtil db = new DBUtil();
		ResultSet rs = DBUtil.getResultset();
		if (rs != null) {
			while (rs.next()) {
				System.out.println("hello");
			}
		}
		else {
			System.out.println("No results found!");
		}
	}
}
