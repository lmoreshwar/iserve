package com.ui.automation.jira;

public final class Constants {

	public static final String JIRA_URL = "https://pact-jiratest-dhhs.ne.gov";

	public static final String JIRA_USERNAME = "";
	public static final String JIRA_PASSWORD = "";
	public static final String JIRA_PROJECT = "";
}