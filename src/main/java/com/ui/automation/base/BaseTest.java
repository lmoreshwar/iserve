package com.ui.automation.base;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.TestDataPrefix;
import com.ui.automation.utilities.ConfigManager;
import com.ui.automation.utilities.ScreenRecorderUtil;

/**
 * @author Eyasu
 *
 */
public class BaseTest {
	public String URL;
	public DesiredCapabilities capibilities;
	public static int EXPLICIT_WAIT_TIME;
	public static String browserName;
	public static ITestContext itc;
	public static File file = null;
	public static Map<String, String> testDataPrefix = new HashMap<>();
	public static String startDate = null;
	public static String suiteName = "";
	public static String chartName = null;
	public static ExtentReports extent;
	public static ExtentTest reportLogger;
	public static RemoteWebDriver webDriver;
	public Boolean highlightElement = Boolean.valueOf(ConfigManager.getProperty("is_highlight_element_enable").trim());
	public String filePath = System.getProperty("user.dir") + "\\DownloadedFiles";
	public String resultsPath = "";
	public static EventFiringWebDriver driver = null;

	/**
	 * @param context It is ITestContext variable to get parameter values passed through from XML file.
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	@BeforeSuite(alwaysRun = true)
	public void setup(ITestContext context) throws Throwable {
		try {
			testDataPrefix =TestDataPrefix.testDataPrefix();
			System.out.println("==========test=============");
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMM_dd_yyyy_z_HH_mm_ss");
			dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
			startDate = dateFormat.format(new Date());
			Map<String, String> suiteParameters = context.getCurrentXmlTest().getSuite().getParameters();
			browserName = suiteParameters.get("browser");
			EXPLICIT_WAIT_TIME = Integer.parseInt(suiteParameters.get("ExplicitWaitTime"));
			suiteName = context.getCurrentXmlTest().getSuite().getName().replace(" ", "_").trim() + "_";
			resultsPath = System.getProperty("user.dir") + File.separator + "Reports" + File.separator + suiteName
					+ startDate + ".html";
			extent = new ExtentReports(resultsPath);
			if (ConfigManager.getProperty("local_machine").equalsIgnoreCase("No")) {
				if (!(ConfigManager.getProperty("cloud_mobile").equalsIgnoreCase("Yes")
						|| ConfigManager.getProperty("cloud_desktop").equalsIgnoreCase("Yes"))) {
					System.out.println("Please select the device for performing automation execution on cloud");
					System.exit(1);
				}
				if (ConfigManager.getProperty("cloud_mobile").equalsIgnoreCase("Yes")
						&& ConfigManager.getProperty("cloud_desktop").equalsIgnoreCase("Yes")) {
					System.out.println(
							"Please select mobile-only or desktop-only for performing automation execution on cloud");
					System.exit(1);
				}
				if (ConfigManager.getProperty("cloud_mobile").equalsIgnoreCase("Yes")) {
					URL = "http://" + ConfigManager.getProperty("cloud_username") + ":"
							+ ConfigManager.getProperty("access_key") + "@hub-cloud.browserstack.com/wd/hub";
					capibilities = new DesiredCapabilities();
					capibilities.setCapability("os_version", ConfigManager.getProperty("version"));
					capibilities.setCapability("device", ConfigManager.getProperty("device"));
					capibilities.setCapability("real_mobile", "true");
					capibilities.setCapability("browserstack.local", "false");
					System.out.println("Connecting to cloud and invoking browser....");
					webDriver = new RemoteWebDriver(new URL(URL), capibilities);
					driver = new EventFiringWebDriver(webDriver);
				}
				if (ConfigManager.getProperty("Cloud_Desktop").equalsIgnoreCase("Yes")) {
					URL = "http://" + ConfigManager.getProperty("cloud_username") + ":"
							+ ConfigManager.getProperty("access_key") + "@hub-cloud.browserstack.com/wd/hub";
					capibilities = new DesiredCapabilities();
					capibilities.setCapability("os", ConfigManager.getProperty("os"));
					capibilities.setCapability("os_version", ConfigManager.getProperty("os_version"));
					capibilities.setCapability("browser", ConfigManager.getProperty("browser"));
					capibilities.setCapability("browser_version", ConfigManager.getProperty("browser_version"));
					capibilities.setCapability("browserstack.local", "false");
					capibilities.setCapability("browserstack.selenium_version", "3.14.0");
					System.out.println("Connecting to cloud and invoking browser....");
					webDriver = new RemoteWebDriver(new URL(URL), capibilities);
					driver = new EventFiringWebDriver(webDriver);
				}
			}
			if (Boolean.valueOf(ConfigManager.getProperty("deletescreenshorts").trim())) {
				ActionEngine.deleteAllFilesFromFolder();
			}
		} catch (Exception ex) {
		}
	}

	/**
	 * @param method  This reference variable gives current method name.
	 * @param ctx     is a ITestContext reference variable.
	 * @param browser This is used to get the browser name.
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	@Parameters({ "browser" })
	@BeforeMethod(alwaysRun = true)
	public void reportHeader(Method method, ITestContext ctx, String browser) throws Throwable {
		itc = ctx;
		BasicConfigurator.configure();
		//ScreenRecorderUtil.startRecording(method.getName());
		if (ConfigManager.getProperty("local_machine").equalsIgnoreCase("Yes")) {
			if (browser.equals("chrome")) {
				System.setProperty("webdriver.chrome.driver", "../iserve-testing/Drivers/chromedriver.exe");
				System.setProperty("webdriver.chrome.silentOutput", "true");
				ChromeOptions options = new ChromeOptions();
				options.setAcceptInsecureCerts(true);
				options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
				options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				options.addArguments("--disable-application-cache");
				options.addArguments("--disable-notifications");
				options.addArguments("--disable-extensions");
				options.addArguments("--disable-infobar");
				// options.addArguments("--headless");
				/*
				 * HashMap<String, Object> chromePrefs = new HashMap<>();
				 * chromePrefs.put("profile.default_content_settings.popups", 0);
				 * chromePrefs.put("download.default_directory", filePath);
				 * chromePrefs.put("plugins.always_open_pdf_externally", true); options.setExperimentalOption("prefs",
				 * chromePrefs); webDriver = new ChromeDriver(options);
				 */

				// ChromeOptions options = new ChromeOptions ();
				// options.addExtensions (new File("filePathExtension.txt"));
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				webDriver = new ChromeDriver(capabilities);
				webDriver.navigate().to("https://registration.telangana.gov.in/UnitRateMV/getDistrictList.htm");
				//webDriver.navigate().to("https://freecrm.com/");
			} else if (browser.equalsIgnoreCase("ie")) {
				// System Property for IEDriver
				System.setProperty("webdriver.ie.driver", "G:\\07_24_2021\\uiautomation\\Drivers\\IEDriverServer.exe");

				// Instantiate a IEDriver class.
				webDriver = new InternetExplorerDriver();
				driver.navigate().to("https://freecrm.com/");
			} else if (browser.equalsIgnoreCase("firefox")) {

			} else if (browser.equals("docker")) {
				capibilities = new DesiredCapabilities();
				capibilities.setCapability(CapabilityType.BROWSER_NAME, BrowserType.CHROME);
				capibilities.setCapability(CapabilityType.PLATFORM_NAME, Platform.LINUX);
				URL url = new URL("http://localhost:4444/wd/hub");
				webDriver = new RemoteWebDriver(url, capibilities);
			}

		}
		try {
			webDriver.manage().window().maximize();
			webDriver.manage().deleteAllCookies();
			webDriver.manage().timeouts().implicitlyWait(EXPLICIT_WAIT_TIME, TimeUnit.SECONDS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to record the screen while scripts are running.
	 */
	@AfterMethod
	public void flush() {
		try {
			//ScreenRecorderUtil.stopRecording();
		} catch (Exception e) {
			e.printStackTrace();
		}
		extent.flush();
		webDriver.close();
	}

	/**
	 * This method closes all open browser windows.
	 *
	 * @throws Exception It will throw exception if any abnormal situation occur while executing the method code.
	 */
	@AfterTest
	public void teardown() throws Exception {
		webDriver.quit();
	}

	/**
	 * This method open the Extent report generated in a default browser after suite execution completed.
	 *
	 */
	@AfterSuite
	public void SuiteClose() {
		File file = new File(resultsPath);
		Desktop desktop = Desktop.getDesktop();
		try {
			desktop.browse(file.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
