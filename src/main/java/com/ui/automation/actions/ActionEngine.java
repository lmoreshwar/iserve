package com.ui.automation.actions;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.common.base.Function;
import com.relevantcodes.extentreports.LogStatus;
import com.ui.automation.base.BaseTest;

/**
 * @author Eyasu
 *
 */
public class ActionEngine extends BaseTest {

	/**
	 * This method is used to refresh the current page.
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void refreshPage() throws Throwable {
		webDriver.navigate().refresh();
		waitForPageToLoad();
	}
	
	 public int getElementsSize(By locator, WebDriver... spDrivers) {
		 //driver = new EventFiringWebDriver(webDriver);
	        int a = 0;
	        EventFiringWebDriver driver1 = driver;
	        if (spDrivers.length != 0) {
	            driver1 = (EventFiringWebDriver) (spDrivers[0]);
	        }
	        try {
	            List<WebElement> rows = driver1.findElements(locator);
	            a = rows.size();
	        } catch (Exception e) {
	            e.toString();
	        }
	        return a;
	    }
	 
	 public int getElementsSize(By locator, WebDriver driver) {
		 //driver = new EventFiringWebDriver(webDriver);
	        int a = 0;
	        try {
	            List<WebElement> rows = driver.findElements(locator);
	            a = rows.size();
	        } catch (Exception e) {
	            e.toString();
	        }
	        return a;
	    }

	 

	/**
	 * This method is used to navigate to previous from current page.
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void navigateBack() throws Throwable {
		webDriver.navigate().back();
		waitForPageToLoad();
	}

	/**
	 * This method will be called when we want get a web element object by passing xpath/id/css etc. of web element.
	 *
	 * @param locator It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @return This method return the web element object.
	 */
	public WebElement getWebElement(By locator) {
		return webDriver.findElement(locator);
	}
	
	

	/**
	 * This method is used to get list of web elements by passing xpath/id/css etc. of web element.
	 *
	 * @param locator It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @return This method return list of web elements.
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public List<WebElement> getWebElements(By locator) throws Throwable {
		List<WebElement> webElements = webDriver.findElements(locator);
		return webElements;
	}

	/**
	 * This method is used to scroll page
	 *
	 * @param pixelValue
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void scrollPage(int pixelValue) throws Throwable {
		// + value for scroll down and -ve value for scroll up
		JavascriptExecutor js = webDriver;
		js.executeScript("window.scrollBy(0," + pixelValue + ")", "");
	}

	/**
	 * This method is used to scroll to specific element
	 *
	 * @param locator It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void scrollPageIntoView(By locator) throws Throwable {
		JavascriptExecutor js = webDriver;
		WebElement element = getWebElement(locator);
		// This will scroll the page Horizontally till the element is found
		js.executeScript("arguments[0].scrollIntoView();", element);
	}

	/**
	 * This method is used to generate random number
	 *
	 * @param min a minimum value
	 * @param max a maximum value
	 * @return a random number
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public int getRandomNumberInRange(int min, int max) throws Throwable {
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	/**
	 * This method is used to get alphanumeric random value
	 *
	 * @param count an numeric value
	 * @return an alphanumeric
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public String getRandomAlphaNumericString(int count) throws Throwable {
		String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}

	/**
	 * This method is used to highlight element
	 *
	 * @param webElement an webobject
	 */
	public void highlightElement(WebElement webElement) {
		if (highlightElement) {
			JavascriptExecutor js = webDriver;
			js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",
					webElement);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", webElement);
		}
	}

	/**
	 * This method is used to return days after specific date
	 *
	 * @param format     an date format
	 * @param futureDays number of future days
	 * @return future date
	 */
	public static String getDateAfterDays(String format, int futureDays) {
		String retDate = null;
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.DATE, futureDays);
			SimpleDateFormat objSDF = new SimpleDateFormat(format);
			retDate = objSDF.format(calendar.getTime());
		} catch (Exception e) {
		}
		return retDate;
	}

	/**
	 * This method is used to return date after specific months
	 *
	 * @param format       date format
	 * @param futureMonths number of months
	 * @return future months date
	 */
	public static String getDateAfterMonths(String format, int futureMonths) {
		String retDate = null;
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.MONTH, futureMonths);
			SimpleDateFormat objSDF = new SimpleDateFormat(format);
			retDate = objSDF.format(calendar.getTime());
		} catch (Exception e) {
		}
		return retDate;
	}

	/**
	 * This method is used to get date after specific years
	 *
	 * @param format     date format
	 * @param futureYear number of years
	 * @return future year date
	 */
	public static String getDateAfterYears(String format, int futureYear) {
		String retDate = null;
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date());
			calendar.add(Calendar.YEAR, futureYear);
			String strDateFormat = format;
			SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
			retDate = objSDF.format(calendar.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retDate;
	}

	/**
	 * This method is used to get screenshot of an application page
	 *
	 * @param driver         webdriver name
	 * @param screenshotName name of the screenshot
	 * @return screenshot path
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public static String getScreenshot(WebDriver driver, String screenshotName) throws Throwable {
		String destination = System.getProperty("user.dir");
		try {
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = webDriver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			destination = destination + "/FailedTestsScreenshots/" + screenshotName + dateName + ".png";
			File finalDestination = new File(destination);
			FileUtils.copyFile(source, finalDestination);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return destination;
	}

	/**
	 * This method is used to drag and drop the objects in an page
	 *
	 * @param draggable source object
	 * @param droppable target object
	 * @return an boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean dragAndDrop(By draggable, By droppable) throws Throwable {
		boolean flag = false;
		WebElement drag = null;
		try {
			Actions action = new Actions(webDriver);
			drag = getWebElement(draggable);
			highlightElement(drag);
			WebElement drop = getWebElement(droppable);
			action.dragAndDrop(drag, drop).build().perform();
			highlightElement(drop);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, drag.getText() + " is dropped to target as expected");
			} else
				reportLogger.log(LogStatus.FAIL, drag.getText() + " couldn't be dropped to target as expected");
		}
		return flag;
	}

	/**
	 * This method is used to perform click operation
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void click(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		WebElement webElement = null;
		try {
			waitForElementToBeClickable(locator, 20);
			webElement = getWebElement(locator);
			highlightElement(webElement);
			webElement.click();
//			waitForPageToLoad();
			flag = true;
		} catch (Exception e) {
			flag = false;
			throw new IllegalStateException(
					"Element or Locator '" + locatorName + "' not found to perform click operation");
			// e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS,
						"Sucessfully clicked on '<b style=\"color:blue;\">" + locatorName + "</b>' element");
			} else
				reportLogger.log(LogStatus.FAIL, "Unable to click on element " + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
		}
	}

	/**
	 * This method is used to perform click operation
	 *
	 * @param webElement  locator object
	 * @param locatorName name of the locator
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void click(WebElement webElement, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			highlightElement(webElement);
			webElement.click();
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS,
						"Sucessfully clicked on '<b style=\"color:blue;\">" + locatorName + "</b>' element");
			} else
				reportLogger.log(LogStatus.FAIL, "Unable to click on element " + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
		}
	}

	/**
	 * This method is used to perform click operation with javascript operation
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator.
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void jsClick(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			waitForElementToBeClickable(locator, 20);
			WebElement webElement = getWebElement(locator);
			JavascriptExecutor executor = webDriver;
			highlightElement(webElement);
			executor.executeScript("arguments[0].click();", webElement);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "JavaScript click is sucessful on element " + locatorName);
			} else
				reportLogger.log(LogStatus.FAIL, "JavaScript click is unsucessful on element " + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
		}
	}

	/**
	 * This method is used to perform click operation for any stale objects.
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void staleClick(By locator, String locatorName) throws Throwable {
		try {
			waitForSeconds(3);
			WebElement element = getWebElement(locator);
			JavascriptExecutor executor = webDriver;
			executor.executeScript("arguments[0].click();", element);
		} catch (org.openqa.selenium.StaleElementReferenceException ex) {
			WebElement element = getWebElement(locator);
			JavascriptExecutor executor = webDriver;
			executor.executeScript("arguments[0].click();", element);
		}
	}

	/**
	 * This method is used to perform double click
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean doubleClick(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			fluentWaitForElementPresent(locator, locatorName, 30, 1);
			Actions action = new Actions(webDriver);
			WebElement webElement = getWebElement(locator);
			highlightElement(webElement);
			action.doubleClick(webElement).perform();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Clicked on Text filed '" + locatorName + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, "Unable to double click on '" + locatorName + "'"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return flag;
	}

	/**
	 * This method is used to enter value in text field.
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param testdata    data passed to the field.
	 * @param locatorName name of the locator.
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void type(By locator, String testdata, String locatorName) throws Throwable {
		boolean isTextEntered = false;
		try {
			waitForElementToBeClickable(locator, 20);
			WebElement webElement = getWebElement(locator);
			webElement.click();
			webElement.sendKeys(Keys.CONTROL + "a");
			webElement.sendKeys(Keys.DELETE);
			webElement.clear();
			highlightElement(webElement);
			webElement.sendKeys(testdata);
			isTextEntered = true;
		} catch (Exception e) {
			throw new IllegalStateException("Element not found to set the data for " + locatorName);
			// e.printStackTrace();
		} finally {
			if (isTextEntered) {
				reportLogger.log(LogStatus.PASS,
						"'<b style=\"color:blue;\"> " + testdata + "</b>' is entered into " + locatorName + " field");
			} else {
				reportLogger.log(LogStatus.FAIL, "Unable to type '" + testdata + "' into " + locatorName + " field. "
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Error_Type")));
			}
		}
	}

	/**
	 * This method is used to perform double click and enter text to the field.
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param value       a value passed to the text field
	 * @param locatorName name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean doubleClickAndType(By locator, String value, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement webElement = getWebElement(locator);
			Actions action = new Actions(webDriver);
			highlightElement(webElement);
			action.doubleClick(webElement).sendKeys(value).perform();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Clicked on Text filed '" + locatorName + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, "Unable to double click on '" + locatorName + "'"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return flag;
	}

	/**
	 * This method is used to send data to text field on clip board
	 *
	 * @param strText text passed
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public static void sendKeyBoardData(String strText) throws Throwable {
		try {
			StringSelection stringSelection = new StringSelection(strText);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			Robot robot = new Robot();
			robot.setAutoDelay(1000);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyPress(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to upload the file
	 *
	 * @param path its an file path to upload
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void fileUpload(String path) throws Throwable {
		StringSelection strSelection = new StringSelection(path);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(strSelection, null);
		Robot robot = new Robot();
		robot.delay(300);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.delay(200);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	/**
	 * This method is used to switch multiple browsers using index value
	 *
	 * @param windowIndex An index value of the browser
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void switchToWindowByIndex(int windowIndex) throws Throwable {
		boolean flag = false;
		try {
			ArrayList<String> tabs = new ArrayList<>(webDriver.getWindowHandles());
			if (windowIndex < tabs.size()) {
				webDriver.switchTo().window(tabs.get(windowIndex));
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, " Swithed to window " + windowIndex + " successfully ");
			} else {
				reportLogger.log(LogStatus.FAIL, " Swithed to window " + windowIndex + " failed "
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Error_Type")));
			}
		}
	}

	/**
	 * This method is used to close browser window
	 *
	 * @param windowIndex browser index to close
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void closeWindowByIndex(int windowIndex) throws Throwable {
		boolean flag = false;
		try {
			ArrayList<String> tabs = new ArrayList<>(webDriver.getWindowHandles());
			if (windowIndex < tabs.size()) {
				webDriver.switchTo().window(tabs.get(windowIndex));
				webDriver.close();
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, " Window " + windowIndex + " closed successfully ");
			} else {
				reportLogger.log(LogStatus.FAIL, " Window " + windowIndex + " failed close "
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Error_Type")));
			}
		}
	}

	/**
	 * This method is used to switch frame based on index
	 *
	 * @param frameIndex a frame index
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void switchToFrameByIndex(int frameIndex) throws Throwable {
		boolean flag = false;
		try {
			webDriver.switchTo().frame(frameIndex);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, " Swithed to Frame " + frameIndex + " successfully ");
			} else {
				reportLogger.log(LogStatus.FAIL, " Swithed to Frame " + frameIndex + " failed "
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Error_Type")));
			}
		}
	}

	/**
	 * This method is used to check the presence of web element
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean isElementPresent(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			JavascriptExecutor js = webDriver;
			WebElement webElement = getWebElement(locator);
			highlightElement(webElement);
			js.executeScript("arguments[0].setAttribute('style','border: solid 5px yellow;');", webElement);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, locatorName + " is present on the page");
			} else {
				reportLogger.log(LogStatus.FAIL, locatorName + " is not present on the page" + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return flag;
	}

	/**
	 * This method is used to check the displaying of web element on web page
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean isElementDisplayed(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebElement webElement = getWebElement(locator);
			flag = webElement.isDisplayed();
			highlightElement(webElement);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, locatorName + " is displayed on the page");
			} else {
				reportLogger.log(LogStatus.FAIL, locatorName + "is not displayed on the page" + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return flag;
	}

	/**
	 * This method is used to wait for the web page to load
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void waitForPageToLoad() throws Throwable {
		String s = "";
		waitForSeconds(1);
		while (!s.equals("complete")) {
			s = (String) ((JavascriptExecutor) webDriver).executeScript("return document.readyState", new Object[0]);
			try {
				waitForSeconds(1);
			} catch (InterruptedException ie) {
				throw ie;
			}
		}
	}

	/**
	 * This method is used to wait for the specific seconds
	 *
	 * @param seconds number of seconds to wait
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void waitForSeconds(int seconds) throws Throwable {
		try {
			Thread.sleep(seconds * 1000L);
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
	}

	/**
	 * This method is used wait for the element to be clickable on page
	 *
	 * @param locator  It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param withTime its an maximum time to wait *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void waitForElementToBeClickable(By locator, int withTime) throws Throwable {
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, withTime);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to wait till the web element locate with frequency set.
	 *
	 * @param locator      It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css
	 *                     etc.
	 * @param locatorName  name of the locator
	 * @param withTimeout  maximum time to wait
	 * @param pollingEvery time intervals
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void fluentWaitForElementPresent(By locator, String locatorName, int withTimeout, int pollingEvery)
			throws Throwable {
		boolean flag = false;
		try {
			Wait<RemoteWebDriver> wait = new FluentWait<>(webDriver).withTimeout(Duration.ofSeconds(withTimeout))
					.pollingEvery(Duration.ofSeconds(pollingEvery)).ignoring(WebDriverException.class);

			wait.until(new Function<WebDriver, WebElement>() {
				@Override
				public WebElement apply(WebDriver driver) {
					return getWebElement(locator);
				}
			});
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Element '" + locatorName + "' is located successfully");
			} else {
				reportLogger.log(LogStatus.FAIL, "Falied to locate element '" + locatorName + "'");
			}
		}
	}

	/**
	 * This method is used to wait for the visibility of element on web page
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator
	 * @param timeToWait  a maximum time to wait
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean waitForVisibilityOfElementLocated(By locator, String locatorName, int timeToWait) throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, timeToWait);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Element is visible with the name '" + locatorName + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, "Element is not visible with the name  '" + locatorName + "'"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return flag;
	}

	/**
	 * This method is used to wait for the element to be present on the page
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean waitForElementPresent(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, 30);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			highlightElement(getWebElement(locator));
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Locator is found for '" + locatorName + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, "Locator is not found for '" + locatorName + "'"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return flag;
	}

	/**
	 * This method is used to hit the KEY from the keyboard
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param KeyName     a name of the key
	 * @param locatorName locator name
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean hitKeyBoardEnterKey(By locator, String KeyName, String locatorName) throws Throwable {
		boolean isKeBoardEnterKeyPressed = false;
		try {
			waitForElementToBeClickable(locator, 30);
			getWebElement(locator).sendKeys(Keys.ENTER);
			isKeBoardEnterKeyPressed = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (isKeBoardEnterKeyPressed) {
				reportLogger.log(LogStatus.PASS, "Pressed '" + KeyName + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, "Pressed'" + KeyName + "'"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return isKeBoardEnterKeyPressed;
	}

	/**
	 * This method is used to download a file
	 *
	 * @param location file path
	 * @throws It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public static void downloadLocation(String location) throws InterruptedException {
		try {
			StringSelection stringSelection = new StringSelection(location);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			Robot robot = new Robot();
			robot.setAutoDelay(2000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyPress(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to click the web object and wait for the expected element to be present
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param waitElement a web element locator
	 * @param locatorName name of the locator
	 * @param timeToWait  a maximum time to wait for the element
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean clickAndWaitForElementPresent(By locator, By waitElement, String locatorName, int timeToWait)
			throws Throwable {
		boolean flag = false;
		try {
			click(locator, locatorName);
			waitForVisibilityOfElementLocated(waitElement, locatorName, timeToWait);
			waitForPageToLoad();
			highlightElement(getWebElement(waitElement));
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Successfully performed clickAndWaitForElementPresent action");
			} else {
				reportLogger.log(LogStatus.FAIL, "Failed to perform clickAndWaitForElementPresent action"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));

			}
		}
		return flag;
	}

	/**
	 * This method is used select a dropdown value based on index
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param index       a index value
	 * @param locatorName name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean selectByIndex(By locator, int index, String locatorName) throws Throwable {
		boolean isByIndexSelected = false;
		try {
			fluentWaitForElementPresent(locator, locatorName, 10, 1);
			WebElement webElement = getWebElement(locator);
			highlightElement(webElement);
			Select dropDown = new Select(webElement);
			dropDown.selectByIndex(index);
			isByIndexSelected = true;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			if (isByIndexSelected) {
				reportLogger.log(LogStatus.PASS, " Option at index '<b style=\"color:blue;\"> " + index
						+ "</b>' is selected from the drop down " + locatorName);
			} else {
				reportLogger.log(LogStatus.FAIL, "Option at index " + index + " is Not selected from the drop dpwn "
						+ locatorName + reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return isByIndexSelected;
	}

	/**
	 * This method is used to select a dropdown based on the value passed
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param value       a value to be selected
	 * @param locatorName name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean selectByValue(By locator, String value, String locatorName) throws Throwable {
		boolean isByValueSelected = false;
		try {
			fluentWaitForElementPresent(locator, locatorName, 30, 1);
			WebElement webElement = getWebElement(locator);
			highlightElement(webElement);
			Select dropDown = new Select(webElement);
			dropDown.selectByValue(value);
			isByValueSelected = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (isByValueSelected) {
				reportLogger.log(LogStatus.PASS,
						"'<b style=\"color:blue;\">" + value + "</b>' is selected from the drop down " + locatorName);
			} else {
				reportLogger.log(LogStatus.FAIL, value + " is not selected from the drop down" + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return isByValueSelected;
	}

	/**
	 * This method is used to select a dropdown based on the text passed
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param visibletext a visible text for the selection
	 * @param locatorName name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean selectByVisibleText(By locator, String visibletext, String locatorName) throws Throwable {
		boolean isVisibleTestSelected = false;
		try {
			fluentWaitForElementPresent(locator, locatorName, 30, 1);
			WebElement webElement = getWebElement(locator);
			highlightElement(webElement);
			Select dropDown = new Select(webElement);
			dropDown.selectByVisibleText(visibletext);
			isVisibleTestSelected = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (isVisibleTestSelected) {
				reportLogger.log(LogStatus.PASS, "'<b style=\"color:blue;\">" + visibletext
						+ "</b>'  is selected from the drop down '" + locatorName + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, "'" + visibletext + "' is not select from the drop down " + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return isVisibleTestSelected;
	}

	/**
	 * This method is used to select a drop down with click operation
	 *
	 * @param dropDownLocator locator of the dropdown
	 * @param valueLocator    a value of the locator
	 * @param locatorName     name of the locator
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void selectValueFromDropDown(By dropDownLocator, String dropDownLocatorName, By valueLocator,
			String valueLocatorName) throws Throwable {
		try {
			click(dropDownLocator, dropDownLocatorName);
			click(valueLocator, valueLocatorName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to select all the values in the drop down
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName name of the locator
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void selectAllOptions(By locator, String locatorName) throws Throwable {
		boolean isAllOptionsSelected = false;
		try {
			fluentWaitForElementPresent(locator, locatorName, 30, 1);
			WebElement webElement = getWebElement(locator);
			highlightElement(webElement);
			Select dropDown = new Select(webElement);
			List<WebElement> allOptions = dropDown.getOptions();
			int totlOptions = allOptions.size();
			for (int i = 0; i < totlOptions; i++) {
				dropDown.selectByIndex(i);
			}
			isAllOptionsSelected = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (isAllOptionsSelected) {
				reportLogger.log(LogStatus.PASS,
						"'<b style=\"color:blue;\"> All options are selected from the drop down" + locatorName
								+ "</b>'");
			} else {
				reportLogger.log(LogStatus.FAIL, "All options are not selected from the drop down" + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
	}

	/**
	 * This method is used to accept the alerts on the web page
	 *
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean acceptAlert() throws Throwable {
		boolean isAlertAccepted = false;
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, 5);
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());
			alert.accept();
			isAlertAccepted = true;
		} catch (NoAlertPresentException ex) {
			ex.printStackTrace();
		} finally {
			if (isAlertAccepted) {
				reportLogger.log(LogStatus.PASS, "<b style=\"color:blue;\">Alet accepted</b>");
			} else {
				reportLogger.log(LogStatus.FAIL, "Failed to accept alert"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Failed to accept alert!")));
			}
		}
		return isAlertAccepted;
	}

	/**
	 * This method is used to dismiss the alerts on the web page
	 *
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean dismissAlert() throws Throwable {
		boolean isAlertDismissed = false;
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, 5);
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());
			alert.dismiss();
			isAlertDismissed = true;
		} catch (NoAlertPresentException ex) {
			ex.printStackTrace();
		} finally {
			if (isAlertDismissed) {
				reportLogger.log(LogStatus.PASS, "Alet dismissed!");
			} else {
				reportLogger.log(LogStatus.FAIL, "Failed to dismiss alert!"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Failed to dismiss alert!")));
			}
		}
		return isAlertDismissed;
	}

	/**
	 * This method is used to retrieve text from the alert box
	 *
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public String getTextFromAlert() throws Throwable {
		String alertText = "";
		boolean isAlertPresent = false;
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, 5);
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());
			alertText = alert.getText();
		} catch (NoAlertPresentException ex) {
			ex.printStackTrace();
		} finally {
			if (isAlertPresent) {
				reportLogger.log(LogStatus.PASS, alertText + " is took from alert box");
			} else {
				reportLogger.log(LogStatus.FAIL, "Failed to get message from alert!"
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Failed to message from alert!")));
			}
		}
		return alertText;
	}

	/**
	 * This method is used to enter text into the alert box
	 *
	 * @param text a content to be passed
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void enterTextIntoAlertBox(String text) throws Throwable {
		boolean isTextEnetered = false;
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, 10);
			Alert alert = wait.until(ExpectedConditions.alertIsPresent());
			alert.sendKeys(text);
			isTextEnetered = true;
		} catch (NoAlertPresentException ex) {
			ex.printStackTrace();
		} finally {
			if (isTextEnetered) {
				reportLogger.log(LogStatus.PASS, text + " is entered into alert box");
			} else {
				reportLogger.log(LogStatus.FAIL, "Failed to enter text into alert box" + reportLogger
						.addScreenCapture(getScreenshot(webDriver, "Failed to enter text into alert box")));
			}
		}
	}

	/**
	 * This method is used to launch URL on the browser
	 *
	 * @param url a URL to passed
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void launchUrl(String url) throws Throwable {
		boolean flag = false;
		try {
			webDriver.manage().deleteAllCookies();
			webDriver.get(url);
			waitForPageToLoad();
			if (webDriver.getCurrentUrl().contains(url)) {
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS,
						"URL '<b style=\"color:blue;\">" + url + "'</b> launched sucessfully.");
			} else {
				reportLogger.log(LogStatus.FAIL,
						"Failed to launch URL " + url + reportLogger.addScreenCapture(getScreenshot(webDriver, "URL")));
			}
		}
	}

	/**
	 * This method is used to verify the passed URL with current URL
	 *
	 * @param url a URL passed to web driver
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void navigateToURL(String url) throws Throwable {
		boolean flag = false;
		try {
			webDriver.get(url);
			if (webDriver.getCurrentUrl().contains(url)) {
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS,
						"Navigated to URL '<b style=\"color:blue;\">" + url + "</b>' sucessfully.");
			} else {
				reportLogger.log(LogStatus.FAIL,
						url + " Failed to launch" + reportLogger.addScreenCapture(getScreenshot(webDriver, "URL")));
			}
		}
	}

	/**
	 * This method is used to get the current URL from the browser
	 *
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public String getCurrentURL() throws Throwable {
		String text = "";
		boolean flag = false;
		try {
			text = webDriver.getCurrentUrl();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Current page URL is '" + text + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, "Failed to get current page URL '" + text + "'");
			}
		}
		return text;
	}

	/**
	 * This method is used to verify an element present on the web page
	 *
	 * @param locator It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean isElementPresent(By locator) throws Throwable {
		int elementsCount = 0;
		String elementText = "";
		boolean flag = false;
		try {
			elementsCount = getWebElements(locator).size();
			if (elementsCount == 1) {
				flag = true;
				elementText = getWebElement(locator).getText();
				highlightElement(getWebElement(locator));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS,
						"Element '<b style=\"color:blue;\">" + elementText + "</b>' located sucessfully.");
			} else {
				reportLogger.log(LogStatus.FAIL, " Locator " + elementText + " not present "
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "Element presence")));
			}
		}
		return flag;
	}

	/**
	 * This method is used to put mouse over on the specific web object
	 *
	 * @param locator It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void mouseHoverOnWebElement(By locator) throws Throwable {
		boolean flag = false;
		String locatorName = "";
		WebElement menuOption = null;
		try {
			Actions actions = new Actions(webDriver);
			menuOption = getWebElement(locator);
			locatorName = menuOption.getText();
			actions.moveToElement(menuOption).perform();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Done Mouse hover on " + locatorName);
			} else {
				reportLogger.log(LogStatus.FAIL, "Failed to mouse hover on " + locatorName);
			}
		}
	}

	/**
	 * This method is used to retrieve title from the web page
	 *
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public String getTitle() throws Throwable {
		String text = "";
		boolean flag = false;
		try {
			text = webDriver.getTitle();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Title of the page is '" + text + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, "Title of the page is '" + text + "'");
			}
		}
		return text;
	}

	/**
	 * This method is used to check the text present on the web page
	 *
	 * @param text content to be checked
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean isTextPresent(String text) throws Throwable {
		boolean flag = false;
		try {
			if ((webDriver.getPageSource()).contains(text)) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, "Text is present in the page ");
			} else {
				reportLogger.log(LogStatus.FAIL, "Text is not present in the page ");
			}
		}
		return flag;
	}

	/**
	 * This method is used to get text from the specified web location
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName
	 * @return
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public String getText(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		String text = "";
		try {
			waitForVisibilityOfElementLocated(locator, locatorName, 30);
			if (isElementPresent(locator, locatorName)) {
				JavascriptExecutor js = webDriver;
				WebElement webElement = getWebElement(locator);
				highlightElement(webElement);
				js.executeScript("arguments[0].setAttribute('style','border: solid 5px yellow;');", webElement);
				text = webElement.getText();
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, " Able to get text from " + locatorName);
			} else {
				reportLogger.log(LogStatus.FAIL, " Unable to get text from " + locatorName);
			}
		}
		return text;
	}
	


	/**
	 * This method is used to get value from the dropdown selected
	 *
	 * @param locator It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public String getDropdownSelectedValue(By locator) throws Throwable {
		String selectedValue = "";
		boolean flag = false;
		try {
			waitForElementToBeClickable(locator, 30);
			WebElement webElement = getWebElement(locator);
			highlightElement(webElement);
			Select selectOption = new Select(webElement);
			selectedValue = selectOption.getFirstSelectedOption().getText();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, " Drop down selected value is '" + selectedValue + "'");
			} else {
				reportLogger.log(LogStatus.FAIL, " Failed to get drop down selected value ");
			}
		}
		return selectedValue;
	}

	/**
	 * This method is used to delete all the files from the specified directory
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public static void deleteAllFilesFromFolder() throws Throwable {
		String path = System.getProperty("user.dir") + "\\DownloadedFiles";
		File file = new File(path);
		File[] files = file.listFiles();
		for (File f : files) {
			if (f.isFile() && f.exists()) {
				reportLogger.log(LogStatus.PASS,
						" File found and deleted <b style=\"color:blue;\">" + f.getName() + "</b>");
				f.delete();
			}
		}
	}

	/**
	 * This method is used to upload files to web page
	 *
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public String chooseFileToUpload() throws Throwable {
		String fileName = "";
		String path = System.getProperty("user.dir") + "\\DownloadedFiles";
		File file = new File(path);
		File[] files = file.listFiles();
		for (File f : files) {
			if (f.isFile() && f.exists()) {
				reportLogger.log(LogStatus.PASS, " File found to choose ");
				fileName = f.getName();
			} else {
				reportLogger.log(LogStatus.FAIL, " File Not found to choose ");
			}
		}
		return fileName;
	}

	/**
	 * This method is used to validate text on the web page
	 *
	 * @param locator  It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param expected an text to be verified
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean verifyText(By locator, String expected) throws Throwable {
		boolean isVerifyTextPassed = false;
		String actual = null;
		try {
			waitForElementToBeClickable(locator, 20);
			WebElement webElement = getWebElement(locator);
			actual = webElement.getText().trim();
			highlightElement(webElement);
			assertEquals(actual, expected);
			isVerifyTextPassed = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (isVerifyTextPassed) {
				reportLogger.log(LogStatus.PASS, "Text verified successfully, actual text is <b style=\"color:blue;\">"
						+ actual + "</b> and expected text is <b style=\"color:blue;\">" + expected + "</b>");
			} else {
				reportLogger.log(LogStatus.FAIL,
						"Text verification failed, actual text is <b style=\"color:red;\">" + actual
								+ "</b> and expected text is <b style=\"color:blue;\">" + expected + "</b>"
								+ reportLogger.addScreenCapture(getScreenshot(webDriver, expected)));

			}
		}
		return isVerifyTextPassed;
	}

	/**
	 * This method is used to validate text contains in the given string
	 *
	 * @param locator  It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param expected an text to be verified
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void verifyTextContains(By locator, String expected) throws Throwable {
		boolean isVerifyTextPassed = false;
		String actual = null;
		try {
			waitForElementToBeClickable(locator, 20);
			WebElement webElement = getWebElement(locator);
			scrollPageIntoView(locator);
			actual = webElement.getText().trim();
			highlightElement(webElement);
			assertTrue(actual.contains(expected));
			isVerifyTextPassed = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (isVerifyTextPassed) {
				reportLogger.log(LogStatus.PASS, "Text verified successfully, <b style=\"color:blue;\">" + expected + "</b> is found in "+"<b style=\"color:blue;\">"+actual+"</b>");
			} else {
				reportLogger.log(LogStatus.FAIL,"Text verified failed, <b style=\"color:blue;\">" + expected + "</b> is not found"
								+ reportLogger.addScreenCapture(getScreenshot(webDriver, expected)));

			}
		}
	}

	/**
	 * This method is used to validate text not contains in the given string
	 *
	 * @param locator  It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param expected an text to be verified
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void verifyTextNotContains(By locator, String expected) throws Throwable {
		boolean isVerifyTextPassed = false;
		String actual = null;
		try {
			waitForPageToLoad();
			WebElement webElement = getWebElement(locator);
			scrollPageIntoView(locator);
			actual = webElement.getText().trim();
			highlightElement(webElement);
			assertTrue(actual.contains(expected));
			isVerifyTextPassed = false;
		} catch (Exception e) {
			isVerifyTextPassed = true;
		} finally {
			if (isVerifyTextPassed) {
				reportLogger.log(LogStatus.PASS, "Text verified successfully, <b style=\"color:blue;\">" + expected + "</b> is not found");
			} else {
				reportLogger.log(LogStatus.FAIL,"Text verified failed, <b style=\"color:blue;\">" + expected + "</b> is found in "+"<b style=\"color:blue;\">"+actual+"</b>"
								+ reportLogger.addScreenCapture(getScreenshot(webDriver, expected)));

			}
		}
	}


	/**
	 * This method is used to check the text as not equal
	 *
	 * @param locator  name of the web locator
	 * @param expected an text to be checked
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean verifyNotEqualText(By locator, String expected) throws Throwable {
		boolean isverifyNotEqualTextPassed = false;
		String actual = null;
		try {
			waitForElementToBeClickable(locator, 20);
			WebElement webElement = getWebElement(locator);
			actual = webElement.getText().trim();
			highlightElement(webElement);
			Assert.assertNotEquals(actual, expected);
			isverifyNotEqualTextPassed = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (isverifyNotEqualTextPassed) {
				reportLogger.log(LogStatus.PASS,
						"Text verified successfully, actual text is <b style=\"color:blue;\">" + actual
								+ "</b> and expected text is <b style=\"color:blue;\">" + expected
								+ " both are not equal</b>");
			} else {
				reportLogger.log(LogStatus.FAIL,
						"Text verification failed, actual text is <b style=\"color:red;\">" + actual
								+ "</b> and expected text is <b style=\"color:blue;\">" + expected
								+ " both are equal</b>");
			}
		}
		return isverifyNotEqualTextPassed;
	}

	/**
	 * This method is used to verify the element to be selected on web page
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param locatorName a name of the locator
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean isElementSelected(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			JavascriptExecutor js = webDriver;
			WebElement webElement = getWebElement(locator);
			flag = webElement.isSelected();
			js.executeScript("arguments[0].setAttribute('style','border: solid 5px yellow;');", webElement);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag) {
				reportLogger.log(LogStatus.PASS, locatorName + " is selected on the page");
			} else {
				reportLogger.log(LogStatus.FAIL, locatorName + "is not selected on the page" + locatorName
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, locatorName)));
			}
		}
		return flag;
	}

	/**
	 * This method is used to wait for the web element to be visible on the web page
	 *
	 * @param locator  It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param withTime a maximum time to wait
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void waitForElementToBeVisible(By locator, int withTime) throws Throwable {
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, withTime);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to verify the element displayed on the web page by not printing the report
	 *
	 * @param locator It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean isElementDisplayedWithoutReport(By locator) throws Throwable {
		boolean flag = false;
		try {
			WebElement webElement = getWebElement(locator);
			flag = webElement.isDisplayed();
			highlightElement(webElement);
		} catch (Exception e) {
		}
		return flag;
	}

	/**
	 * This method is used to verify the element present on the web page by not printing the report
	 *
	 * @param locator It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @return a boolean value
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public boolean isElementPresentWithoutReport(By locator) throws Throwable {
		boolean flag = false;
		try {
			WebElement webElement = getWebElement(locator);
			highlightElement(webElement);
			flag = true;
		} catch (Exception e) {
		}
		return flag;
	}

	/**
	 * This method is used to delete existing file from the folder 'DownloadedFiles'
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public static void delteExistingFilesFromFolder() throws Throwable {
		String path = System.getProperty("user.dir") + "\\DownloadedFiles";
		File file = new File(path);
		File[] files = file.listFiles();
		for (File f : files) {
			if (f.isFile() && f.exists()) {
				reportLogger.log(LogStatus.PASS,
						"File found and deleted <b style=\"color:blue;\">" + f.getName() + "</b>");
				f.delete();
			} else {
				reportLogger.log(LogStatus.FAIL, " File Not found to delete "
						+ reportLogger.addScreenCapture(getScreenshot(webDriver, "delteFileDownloadFolder")));
			}
		}
	}
	
	
}