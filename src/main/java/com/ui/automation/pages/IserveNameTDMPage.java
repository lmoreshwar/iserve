package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;

/**
 * @author Douglas
 * @table ISERVE.Applicant
 */
public class IserveNameTDMPage extends ActionEngine {

	private By sFirstName = By.name("firstName");
	private By sMiddleName = By.name("middleName");
	private By sLastName = By.name("lastName");
	private By sSuffix = By.name("suffix");
	private By sAliasYes = By.xpath("//*[@name='isAlias' and @value='Yes']/following-sibling::label");
	private By sAliasNo = By.xpath("//*[@name='isAlias' and @value='No']/following-sibling::label");
	private By sAliasName0 = By.name("alias-0");
	private By sAliasName1 = By.name("alias-1");

	public void setDBData(HashMap<String, String> testData) throws Throwable {

		String dFrameworkID = testData.get("FrameworkID");
		String dFirstName = testData.get("FirstName");
		String dMiddleName = testData.get("MiddleName");
		String dLastName = testData.get("LastName");
		String dSuffix = testData.get("Suffix");
		String dAlias = testData.get("Alias");
		String dAliasName0 = testData.get("AliasName0");
		String dAliasName1 = testData.get("AliasName1");

		try {

			switch (dFirstName) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sFirstName, dFirstName, "First name");
			}

			switch (dMiddleName) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sMiddleName, dMiddleName, "Middle name");
			}

			switch (dLastName) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sLastName, dLastName, "Last name");
			}

			switch (dSuffix) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sSuffix, dSuffix, "Suffix");
			}

			dAlias = (String.valueOf(dAlias).replaceFirst("\\.0+$", ""));
			switch (dAlias) {
			case "0": // No
				click(sAliasNo, "Alias No");
				break;
			case "1": // Yes
				click(sAliasYes, "Alias Yes");
				type(sAliasName0, dAliasName0, "Alias Name 0");
				//type(sAliasName1, dAliasName1, "Alias Name 1");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("End of Page or Success - Name Page - To start, please tell us about yourself");
	}

}