package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IservePrivacyPage extends ActionEngine {

	private By sPrivacyAgree = By.cssSelector(".usa-checkbox__label");

	public void setData(HashMap<String, String> testData) throws Throwable {
		ActionObjects actionobjects = new ActionObjects();
		String dPrivacyAgree = testData.get("PrivacyAgree");

		dPrivacyAgree = (String.valueOf(dPrivacyAgree).replaceFirst("\\.0+$", ""));
		switch (dPrivacyAgree) {
		case "0": // No/Disagree
			break;
		case "1": // Yes/Agree
			click(sPrivacyAgree,"I agree to allow my information to be used and retrieved from data sources for this application.");
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Privacy Page.");

	}
}