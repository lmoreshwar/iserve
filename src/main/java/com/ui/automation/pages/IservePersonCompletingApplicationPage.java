package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IservePersonCompletingApplicationPage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
	private By sApplicantRole1 = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(1) > .usa-radio__label");// I am the Applicant
	private By sApplicantRole2 = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(2) > .usa-radio__label");// I am an authorized	representative.
	private By sApplicantRole3 = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(3) > .usa-radio__label");// I am a parent or legal guardian of the minor.
	private By sApplicantRole4 = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(4) > .usa-radio__label");// I am a guardian, conservator, or power of attorney acting on behalf of the Applicant.
	private By sApplicantRole5 = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(5) > .usa-radio__label");// I am a member of the household listed on the application.
	private By sApplicantRole6 = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(6) > .usa-radio__label");// I am a contracted	Medicaid provider.
	private By sApplicantRole7 = By.cssSelector(".usa-fieldset:nth-child(2) .usa-radio:nth-child(7) > .usa-radio__label");// I am a DHHS worker.

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dApplicantRole1 = testData.get("ApplicantRole1");// I am the Applicant.
		String dApplicantRole2 = testData.get("ApplicantRole2");// I am an authorized representative.
		String dApplicantRole3 = testData.get("ApplicantRole3");// I am a parent or legal guardian of the minor.
		String dApplicantRole4 = testData.get("ApplicantRole4");// I am a guardian, conservator, or power of attorney acting on behalf of the Applicant.
		String dApplicantRole5 = testData.get("ApplicantRole5");// I am a member of the household listed on the application.
		String dApplicantRole6 = testData.get("ApplicantRole6");// I am a contracted Medicaid provider.
		String dApplicantRole7 = testData.get("ApplicantRole7");// I am a DHHS worker.

		try {

			dApplicantRole1 = (String.valueOf(dApplicantRole1).replaceFirst("\\.0+$", ""));
			switch (dApplicantRole1) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRole1, " - I am the Applicant.");
			}

			dApplicantRole2 = (String.valueOf(dApplicantRole2).replaceFirst("\\.0+$", ""));
			switch (dApplicantRole2) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRole2, " - I am an authorized representative.");
			}
			dApplicantRole3 = (String.valueOf(dApplicantRole3).replaceFirst("\\.0+$", ""));
			switch (dApplicantRole3) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRole3, " - I am a parent or legal guardian of the minor.");
			}

			dApplicantRole4 = (String.valueOf(dApplicantRole4).replaceFirst("\\.0+$", ""));
			switch (dApplicantRole4) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRole4, " - I am a guardian, conservator, or power of attorney acting on behalf of the Applicant.");
			}

			dApplicantRole5 = (String.valueOf(dApplicantRole5).replaceFirst("\\.0+$", ""));
			switch (dApplicantRole5) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRole5, " - I am a member of the household listed on the application.");
			}

			dApplicantRole6 = (String.valueOf(dApplicantRole6).replaceFirst("\\.0+$", ""));
			switch (dApplicantRole6) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRole6, " - I am a contracted Medicaid provider.");
			}

			dApplicantRole7 = (String.valueOf(dApplicantRole7).replaceFirst("\\.0+$", ""));
			switch (dApplicantRole7) {
			case "0": // No/
				break;
			case "1": // Yes/
				click(sApplicantRole7, " - I am a DHHS worker.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Person Completing Application Page - Document Upload Application Submission End.");

	}

}