package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveAuthorizedRepresentativeAddressPage extends ActionEngine {

	private By sAdd1 = By.name("authorizedRepAddress.line_1");
	private By sAdd2 = By.name("authorizedRepAddress.line_2");
	private By sCity = By.name("authorizedRepAddress.city");
	private By sCounty = By.name("authorizedRepAddress.county");
	private By sState = By.name("authorizedRepAddress.state");
	private By sPostalCode5 = By.name("authorizedRepAddress.zip5");
	private By sPostalCode4 = By.name("authorizedRepAddress.zip4");

	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects actionobjects = new ActionObjects();
		String dAdd1 = testData.get("Add1");
		String dAdd2 = testData.get("Add2");
		// String add3 = testData.get("add3");
		String dCity = testData.get("City");
		String dCounty = testData.get("County");
		String dState = testData.get("StateAbbreviation");
		String dPostalCode5 = testData.get("PostalCode5");
		dPostalCode5 = (String.valueOf(dPostalCode5).replaceFirst("\\.0+$", ""));
		String dPostalCode4 = testData.get("PostalCode4");
		dPostalCode4 = (String.valueOf(dPostalCode4).replaceFirst("\\.0+$", ""));

		try {

			switch (dAdd1) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sAdd1, dAdd1, "Street Address 1");
			}

			switch (dAdd2) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sAdd2, dAdd2, "Street Address 2 (Optional)");
			}

			switch (dCity) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sCity, dCity, "City");
			}

			switch (dState) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				selectByValue(sState, dState, "State");
			}

			switch (dCounty) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				selectByValue(sCounty, dCounty, "County");
			}

			switch (dPostalCode5) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sPostalCode5, dPostalCode5, "PostalCode5");
			}

			switch (dPostalCode4) {
			case "null":
				break;
			case "":
				break;
			case "0":
				break;
			default:
				type(sPostalCode4, dPostalCode4, "PostalCode4");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Authorized Representative Address Page - Provide contact information for this authorized representative.");

	}

}