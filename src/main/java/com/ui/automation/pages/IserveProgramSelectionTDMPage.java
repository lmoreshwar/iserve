package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.ProgramSelection
 */
public class IserveProgramSelectionTDMPage extends ActionEngine {

	final By sMedicaid = By.cssSelector(".usa-form-group:nth-child(1) > .usa-checkbox__label");
	final By sHcbs = By.cssSelector(".usa-form-group:nth-child(2) > .usa-checkbox__label");
	final By sSnap = By.cssSelector(".usa-form-group:nth-child(3) > .usa-checkbox__label");
	final By sLiheap = By.cssSelector(".usa-form-group:nth-child(4) > .usa-checkbox__label");
	final By sRrp = By.cssSelector(".usa-form-group:nth-child(5) > .usa-checkbox__label");
	final By sAdc = By.cssSelector(".usa-form-group:nth-child(6) > .usa-checkbox__label");
	final By sCcs = By.cssSelector(".usa-form-group:nth-child(7) > .usa-checkbox__label");
	final By sEma = By.cssSelector(".usa-form-group:nth-child(8) > .usa-checkbox__label");
	final By sAabd = By.cssSelector(".usa-form-group:nth-child(9) > .usa-checkbox__label");
	final By sSdp = By.cssSelector(".usa-form-group:nth-child(10) > .usa-checkbox__label");
	final By sSsad = By.cssSelector(".usa-form-group:nth-child(11) > .usa-checkbox__label");
	final By sPas = By.cssSelector(".usa-form-group:nth-child(12) > .usa-checkbox__label");

	public void setDBData(HashMap<String, String> testData) throws Throwable {

		ActionObjects actionobjects = new ActionObjects();
		String dMedicaid = testData.get("Medicaid");
		String dHcbs = testData.get("HCBS");
		String dSnap = testData.get("SNAP");
		String dLiheap = testData.get("LIHEAP");
		String dRrp = testData.get("RRP");
		String dAdc = testData.get("ADC");
		String dCcs = testData.get("CCS");
		String dEma = testData.get("EMA");
		String dAabd = testData.get("AABD");
		String dSdp = testData.get("SDP");
		String dSsad = testData.get("SSAD");
		String dPas = testData.get("PAS");

		try {

			switch (dMedicaid) {
			case "0": // No
				break;
			case "1": // Yes
				click(sMedicaid, "Medicaid");
			}

			switch (dHcbs) {
			case "0": // No
				break;
			case "1": // Yes
				click(sHcbs, "HCBS - Home and Community Based Services");
			}

			switch (dSnap) {
			case "0": // No
				break;
			case "1": // Yes
				click(sSnap, "SNAP - Supplemental Nutrition Assistance Program (SNAP)");
			}

			switch (dLiheap) {
			case "0": // No
				break;
			case "1": // Yes
				click(sLiheap, "LIHEAP - Low Income Home Energy Assistance Program");
			}

			switch (dRrp) {
			case "0": // No
				break;
			case "1": // Yes
				click(sRrp, "RRP - Refugee Resettlement Program");
			}

			switch (dAdc) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAdc, "ADC - Aid to Dependent Children");
			}

			switch (dCcs) {
			case "0": // No
				break;
			case "1": // Yes
				click(sCcs, "CCS - Child Care Subsidy");
			}

			switch (dEma) {
			case "0": // No
				break;
			case "1": // Yes
				click(sEma, "EMA - Emergency Assistance");
			}

			switch (dAabd) {
			case "0": // No
				break;
			case "1": // Yes
				click(sAabd, "AABD - Assistance to the Aged");
			}

			switch (dSdp) {
			case "0": // No
				break;
			case "1": // Yes
				click(sSdp, "SDP - State Disability Program");
			}

			switch (dSsad) {
			case "0": // No
				break;
			case "1": // Yes
				click(sSsad, "SSAD - Social Services for the Aged and / or Disabled");
			}

			switch (dPas) {
			case "0": // No
				break;
			case "1": // Yes
				click(sPas, "PAS - Personal Assistance Services");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		actionobjects.bSaveAndContinue();
		System.out
				.println("End of Page or Success - Program Selection Page - Please select one or more programs below.");
	}

}