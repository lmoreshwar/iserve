package com.ui.automation.pages;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;

import com.relevantcodes.extentreports.LogStatus;
import com.ui.automation.actions.ActionEngine;
import com.ui.automation.utilities.GetScreenShot;

/**
 * @author Douglas
 * @table ISERVE.?
 *
 */
public class IserveBenefitDiscoveryBenefitResultsPage extends ActionEngine {


	public void selectBenefitDiscoveryResults() throws Throwable {
		GetScreenShot GetScreenShot = new GetScreenShot();
		waitForPageToLoad();
		com.ui.automation.utilities.GetScreenShot.captureScreenshot(webDriver, startDate);

		System.out.println("End of Page or Success - Benefits Discovery Results.");

	}

	public void selectBenefitDiscoveryResults(HashMap<String, String> testData) throws Throwable {
		waitForPageToLoad();
		String rslt = "//*[contains(text(), '(resultString)')]";
		List<String> listResultYes = Arrays.asList((String.valueOf(testData.get("ResultYes")).replaceFirst("\\.0+$", "")).trim().split("_"));
		List<String> listResultNo = Arrays.asList((String.valueOf(testData.get("ResultNo")).replaceFirst("\\.0+$", "")).trim().split("_"));

		if (listResultYes.size() > 0) {
			for (String rYes : listResultYes) {
				if (!"".equals(rYes)) {
					verifyTextContains(By.xpath(rslt.replace("resultString", rYes)), rYes);
				}
			}

		}

		if (listResultNo.size() > 0) {
			for (String rNo : listResultNo) {
				if (!"".equals(rNo)) {
					verifyTextNotContains(By.xpath(rslt.replace("resultString", rNo)), rNo);
				}
			}
		}

		reportLogger.log(LogStatus.INFO,
				reportLogger.addScreenCapture(getScreenshot(webDriver, testData.get("ResultYes"))));

		System.out.println("End of Page or Success - Benefits Discovery Results.");

	}

}