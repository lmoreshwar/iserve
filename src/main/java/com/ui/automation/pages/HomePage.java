package com.ui.automation.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.gargoylesoftware.htmlunit.javascript.host.Iterator;
import com.relevantcodes.extentreports.LogStatus;
import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;
import com.ui.automation.utilities.ConfigManager;

import edu.emory.mathcs.backport.java.util.concurrent.TimeUnit;

/**
 * @author Eyasu
 *
 */
public class HomePage extends ActionEngine {
	ActionObjects actionobjects = new ActionObjects();
	private By textUserName = By.id("email");
	private By textPassWord = By.id("pass");
	private By buttonLogIn = By.id("loginbutton1");
	private By textSandbox = By.xpath("//span[text()='text']");
	private By imgProfile = By.xpath("//img[@title='User']//parent::node()");
	private By linkLogOut = By.xpath("//a[text()='Log Out']");

	/**
	 * This method is used in launching the Application URL
	 */
	public void launch(String url) {
		try {
			launchUrl(url);
			waitForPageToLoad();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to login into the application
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void login() throws Throwable {
		try {
			type(textUserName, ConfigManager.getProperty("username").trim(), "User Name");
			typePassword(textPassWord, ConfigManager.getProperty("password").trim(), "Password");
			click(buttonLogIn, "Login Button");
			waitForPageToLoad();
			verifyText(textSandbox, "");
		} catch (Exception e) {
			e.getStackTrace()[0].getLineNumber();
			reportLogger.log(LogStatus.FAIL, e.getStackTrace()[0].getLineNumber() + "");
		}
	}

	public void loginApplication() throws Throwable {
		try {
			launch(ConfigManager.getProperty("url").trim());
			login();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to logout from the application
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void logout() throws Throwable {
		try {
			click(imgProfile, "Profile");
			jsClick(linkLogOut, "Logout");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to enter application password value in text field.
	 *
	 * @param locator     It is a locator for web element in the page. Ex. locator can be found with Id, xpath, css etc.
	 * @param testdata    data passed to the field.
	 * @param locatorName name of the locator.
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	private void typePassword(By locator, String testdata, String locatorName) throws Throwable {
		try {
			waitForElementToBeClickable(locator, 20);
			WebElement webElement = getWebElement(locator);
			webElement.click();
			webElement.sendKeys(Keys.CONTROL + "a");
			webElement.sendKeys(Keys.DELETE);
			webElement.clear();
			highlightElement(webElement);
			webElement.sendKeys(testdata);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to access the URL and select Start Benefits Application without credentials
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void noLoginCredentialsStartBenefitsApplication() throws Throwable {
		try {
			launch(ConfigManager.getProperty("url").trim());
			click(actionobjects.bStartBenefitsApplication, "Start Benefits Application");
			click(actionobjects.bStart, "Start Application");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This method is used to access the URL and select Go to Benefits Discovery without credentials
	 *
	 * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
	 */
	public void noLoginCredentialsGoToBenefitDiscovery() throws Throwable {
		try {
			launch(ConfigManager.getProperty("url").trim());
			click(actionobjects.bGoToBenefitsDiscovery, "Go to Benefit Discovery");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void noLoginCredentialsAccessURL() throws Throwable {
		try {
			launch(ConfigManager.getProperty("url").trim());
			waitForPageToLoad();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void verifyLogin() throws Throwable {
		isElementDisplayed(By.xpath("//*[text()='Log In']"), "login button");
		click(By.xpath("//*[text()='Log In']"), "Login");
		type(By.name("email"), "soma.moreshwar@gmail.com", "user name");
		type(By.name("email"), "soma.moreshwar@gmail.com", "user name");
		type(By.name("password"), "Welcome@123", "password");
		click(By.xpath("//div[contains(@class,'submit')][text()='Login']"), "Login button");
		waitForVisibilityOfElementLocated(By.xpath("//*[@class='ui fluid card']//div[@class='header']"), "Tiles Headers", 2);
		/*
		 * List<WebElement> list =
		 * getWebElements(By.xpath("//*[@class='ui fluid card']//div[@class='header']"))
		 * ; Thread.sleep(3); java.util.Iterator<WebElement> itr = list.iterator();
		 * while(itr.hasNext()) { WebElement element = itr.next();
		 * 
		 * // Perform Action System.out.println(element.getText()); }
		 */
		List<WebElement> obj = getWebElements(By.xpath("//*[@class='ui fluid card']//div[@class='header']"));
		List<String> StringObj = new ArrayList<>();


		for(WebElement value:obj) { 
			String tileValue = value.getText();
			StringObj.add(tileValue);
			System.out.println("Value of the Tile is : " + tileValue );
		}
		int count = StringObj.size();
		for(int i=0;i<count;i++) {
			switch (StringObj.get(i)) {
			case "Deals Summary":
				System.out.println("==========Inside Deals Summary ================");
				break;
			case "Sale Targets":
				System.out.println("==========Inside Sales Targets ================");
				break;
			case "Contacts activity stream":
				System.out.println("==========Inside Contacts activity stream ================");
				break;
			case "Deals":
				System.out.println("==========Inside Deals ================");
				break;
			case "Twitter":
				System.out.println("==========Inside Twitter ================");
				break;
			case "change Rates":
				System.out.println("==========Inside change Rates ================");
				break;
			default:
				break;
			}
		}
	}
	
	public void verifyHome(Map<String,String> dataHome){
		
		
	}

}