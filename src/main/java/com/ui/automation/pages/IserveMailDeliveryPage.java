package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveMailDeliveryPage extends ActionEngine {

	private By sMailDeliveryYes = By.cssSelector(".usa-form-group:nth-child(1) > .usa-radio__label");// Yes
	private By sMailDeliveryNo = By.cssSelector(".usa-form-group:nth-child(2) > .usa-radio__label");// No
	private By sCity = By.name("homeAddress.City");
	private By sCounty = By.name("homeAddress.County");
	private By sState = By.name("homeAddress.State");
	private By sPostalCode5 = By.name("homeAddress.zip5");
	private By sPostalCode4 = By.name("homeAddress.zip4");

	public void setData(HashMap<String, String> testData) throws Throwable {

		String dMailDelivery = testData.get("MailDelivery");
		String dCity = testData.get("City");
		String dCounty = testData.get("County");
		String dState = testData.get("StateAbbreviation");
		String dPostalCode5 = testData.get("PostalCode5");
		dPostalCode5 = (String.valueOf(dPostalCode5).replaceFirst("\\.0+$", ""));
		String dPostalCode4 = testData.get("PostalCode4");
		dPostalCode4 = (String.valueOf(dPostalCode4).replaceFirst("\\.0+$", ""));

		dMailDelivery = (String.valueOf(dMailDelivery).replaceFirst("\\.0+$", ""));
		switch (dMailDelivery) {
		case "0": // No
			click(sMailDeliveryNo, "Mail Delviery - No");
			break;
		case "1": // Yes
			click(sMailDeliveryYes, "Mail Delviery - Yes");
			type(sCity, dCity, "City");
			selectByValue(sCounty, dCounty, "County");
			selectByValue(sState, dState, "State");
			type(sPostalCode5, dPostalCode5, "PostalCode5");
			type(sPostalCode4, dPostalCode4, "PostalCode4");
		}

		System.out.println("End of Page or Success - Mail Delivery Page.");
	}

}