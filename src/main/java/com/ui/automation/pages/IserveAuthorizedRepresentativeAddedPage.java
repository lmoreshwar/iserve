package com.ui.automation.pages;

import java.util.HashMap;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveAuthorizedRepresentativeAddedPage extends ActionEngine {



	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects ActionObjects = new ActionObjects();

		ActionObjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Successfully added an authorized representative.");

	}
}