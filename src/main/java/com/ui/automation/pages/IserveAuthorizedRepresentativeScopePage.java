package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table [ISERVE].[HouseholdApplicantInfo]
 *
 */
public class IserveAuthorizedRepresentativeScopePage extends ActionEngine {

	private By sSignApplication = By.xpath("//*[@name='responsibilitiesCheckbox' and @value='sign-application']/following-sibling::label");
	private By sCompleteSubmitRenewalForm = By.xpath("//*[@name='responsibilitiesCheckbox' and @value='complete-submit-renewal-form']/following-sibling::label");
	private By sReceiveNotices = By.xpath("//*[@name='responsibilitiesCheckbox' and @value='receive-notices']/following-sibling::label");
	private By sActOnBehalfAapplicant = By.xpath("//*[@name='responsibilitiesCheckbox' and @value='act-on-behalf-applicant']/following-sibling::label");

	public void setData(HashMap<String, String> testData) throws Throwable {

		ActionObjects ActionObjects = new ActionObjects();
		String dSignApplication = testData.get("SignApplication");
		String dCompleteSubmitRenewalForm = testData.get("CompleteSubmitRenewalForm");
		String dReceiveNotices = testData.get("ReceiveNotices");
		String dActOnBehalfAapplicant = testData.get("ActOnBehalAapplicant");

		try {

			dSignApplication = (String.valueOf(dSignApplication).replaceFirst("\\.0+$", ""));
			switch (dSignApplication) {
			case "0": // No
				break;
			case "1": // Yes
				click(sSignApplication, "Sign Application");
			}

			dCompleteSubmitRenewalForm = (String.valueOf(dCompleteSubmitRenewalForm).replaceFirst("\\.0+$", ""));
			switch (dCompleteSubmitRenewalForm) {
			case "0": // No
				break;
			case "1": // Yes
				click(sCompleteSubmitRenewalForm, "Complete Submit Renewal Form");
			}

			dReceiveNotices = (String.valueOf(dReceiveNotices).replaceFirst("\\.0+$", ""));
			switch (dReceiveNotices) {
			case "0": // No
				break;
			case "1": // Yes
				click(sReceiveNotices, "Receive Notices");
			}

			dActOnBehalfAapplicant = (String.valueOf(dActOnBehalfAapplicant).replaceFirst("\\.0+$", ""));
			switch (dActOnBehalfAapplicant) {
			case "0": // No
				break;
			case "1": // Yes
				click(sActOnBehalfAapplicant, "Act On Behalf of Applicant");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		ActionObjects.bSaveAndContinue();
		System.out.println("End of Page or Success - Authorized Representative Scope Page.");

		}

}