package com.ui.automation.pages;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveLegalTermsPage extends ActionEngine { //Medicaid

	ActionObjects actionobjects = new ActionObjects();
	private By sScroll1 = By.cssSelector(".usa-card__body > div");

	public void setData(HashMap<String, String> testData) throws Throwable {

		//scrollPage(1000);
		click(sScroll1, "Notice of Information Privacy Practices");

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_END);
		robot.keyRelease(KeyEvent.VK_END);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		actionobjects.bIUnderstandAndAgree();
		System.out.println("End of Page or Success - Before you finish, read and agree to the legal terms page");

	}
}