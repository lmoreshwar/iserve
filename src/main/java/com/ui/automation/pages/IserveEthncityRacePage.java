package com.ui.automation.pages;

import java.util.HashMap;

import org.openqa.selenium.By;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;

/**
 * @author Douglas
 * @table ISERVE.?
 * @spreadsheet IServe_TestData.xlsx
 */
public class IserveEthncityRacePage extends ActionEngine {

	ActionObjects actionobjects = new ActionObjects();
    //private By sNotOfHispanicLatinoOrSpanishOrigin = (By.cssSelector(".usa-fieldset:nth-child(3) .usa-checkbox:nth-child(1) > .usa-checkbox__label"));
	private By sNotOfHispanicLatinoOrSpanishOrigin = (By.xpath("//label[contains(.,'Not of Hispanic, Latino, or Spanish origin')]"));
    private By sMexican = (By.cssSelector(".usa-fieldset:nth-child(3) .usa-checkbox:nth-child(2) > .usa-checkbox__label"));
    private By sPuertoRican = (By.cssSelector(".usa-fieldset:nth-child(3) .usa-checkbox:nth-child(3) > .usa-checkbox__label"));
    private By sCentralAmerican = (By.cssSelector(".usa-fieldset:nth-child(3) .usa-checkbox:nth-child(4) > .usa-checkbox__label"));
    private By sCuban = (By.cssSelector(".usa-fieldset:nth-child(3) .usa-checkbox:nth-child(5) > .usa-checkbox__label"));
    private By sSouthAmerican = (By.cssSelector(".usa-fieldset:nth-child(3) .usa-checkbox:nth-child(6) > .usa-checkbox__label"));
    private By sOtherHispanicLatinoOrSpanishOrigin = (By.cssSelector(".usa-fieldset:nth-child(3) .usa-checkbox:nth-child(7) > .usa-checkbox__label"));
    private By sOtherUnknownEthincity = (By.cssSelector(".usa-fieldset:nth-child(3) .usa-checkbox:nth-child(8) > .usa-checkbox__label"));
    private By sOtherUnknownEthincityDescription  = By.name("ethnicityOther");
    private By sBlackAfricanAmerican = (By.cssSelector(".usa-fieldset:nth-child(5) .usa-checkbox:nth-child(1) > .usa-checkbox__label"));
    //private By sWhiteCaucasian = (By.cssSelector(".usa-fieldset:nth-child(5) .usa-checkbox:nth-child(2) > .usa-checkbox__label"));
    private By sWhiteCaucasian = (By.xpath("//label[contains(.,'White/Caucasian')]"));
    private By sAsian = (By.cssSelector(".usa-fieldset:nth-child(5) .usa-checkbox:nth-child(3) > .usa-checkbox__label"));
    private By sAmericanIndian = (By.cssSelector(".usa-fieldset:nth-child(5) .usa-checkbox:nth-child(4) > .usa-checkbox__label"));
    private By sAlaskanNative = (By.cssSelector(".usa-fieldset:nth-child(5) .usa-checkbox:nth-child(5) > .usa-checkbox__label"));
    private By sNativeHawaiian = (By.cssSelector(".usa-fieldset:nth-child(5) .usa-checkbox:nth-child(6) > .usa-checkbox__label"));
    private By sOtherPacificIslander = (By.cssSelector(".usa-fieldset:nth-child(5) .usa-checkbox:nth-child(7) > .usa-checkbox__label"));
    private By sOtherUnknownRace = (By.cssSelector(".usa-fieldset:nth-child(5) .usa-checkbox:nth-child(8) > .usa-checkbox__label"));
    private By sOtherUnknownRaceDescription  = By.name("raceOther");

    /**
     * This method is used to login into the application
     *
     * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
     */
    public void setData(HashMap<String,String> testData) throws Throwable {

    	String dNotOfHispanicLatinoOrSpanishOrigin = testData.get("NotOfHispanicLatinoOrSpanishOrigin");
    	String dMexican = testData.get("Mexican");
        String dPuertoRican = testData.get("PuertoRican");
        String dCentralAmerican = testData.get("CentralAmerican");
        String dCuban = testData.get("Cuban");
        String dSouthAmerican = testData.get("SouthAmerican");
        String dOtherHispanicLatinoOrSpanishOrigin = testData.get("OtherHispanicLatinoOrSpanishOrigin");
        String dOtherUnknownEthincity = testData.get("OtherUnknownEthincity");
        String dOtherUnknownEthincityDescription = testData.get("OtherUnknownEthincityDescription");
        String dBlackAfricanAmerican = testData.get("BlackAfricanAmerican");
        String dWhiteCaucasian = testData.get("WhiteCaucasian");
        String dAsian = testData.get("Asian");
        String dAmericanIndian = testData.get("AmericanIndian");
        String dAlaskanNative = testData.get("AlaskanNative");
        String dNativeHawaiian = testData.get("NativeHawaiian");
        String dOtherPacificIslander = testData.get("OtherPacificIslander");
        String dOtherUnknownRace = testData.get("OtherUnknownRace");
        String dOtherUnknownRaceDescription = testData.get("OtherUnknownRaceDescription");

    	 try {

         	dNotOfHispanicLatinoOrSpanishOrigin = (String.valueOf(dNotOfHispanicLatinoOrSpanishOrigin).replaceFirst("\\.0+$", ""));
 			switch (dNotOfHispanicLatinoOrSpanishOrigin) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sNotOfHispanicLatinoOrSpanishOrigin, "sNotOfHispanicLatinoOrSpanishOrigin");
 			}

 			dMexican = (String.valueOf(dMexican).replaceFirst("\\.0+$", ""));
 			switch (dMexican) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sMexican, "sMexican");
 			}

 			dPuertoRican = (String.valueOf(dPuertoRican).replaceFirst("\\.0+$", ""));
 			switch (dPuertoRican) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sPuertoRican, "sPuertoRican");
 			}

 			dCentralAmerican = (String.valueOf(dCentralAmerican).replaceFirst("\\.0+$", ""));
 			switch (dCentralAmerican) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sCentralAmerican, "sCentralAmerican");
 			}

 			dCuban = (String.valueOf(dCuban).replaceFirst("\\.0+$", ""));
 			switch (dCuban) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sCuban, "sCuban");
 			}

 			dSouthAmerican = (String.valueOf(dSouthAmerican).replaceFirst("\\.0+$", ""));
 			switch (dSouthAmerican) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sSouthAmerican, "sSouthAmerican");
 			}

 			dOtherHispanicLatinoOrSpanishOrigin = (String.valueOf(dOtherHispanicLatinoOrSpanishOrigin).replaceFirst("\\.0+$", ""));
 			switch (dOtherHispanicLatinoOrSpanishOrigin) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sOtherHispanicLatinoOrSpanishOrigin, "sOtherHispanicLatinoOrSpanishOrigin");
 			}

 			dOtherUnknownEthincity = (String.valueOf(dOtherUnknownEthincity).replaceFirst("\\.0+$", ""));
 			switch (dOtherUnknownEthincity) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sOtherUnknownEthincity, "sOtherUnknownEthincity");
 				type(sOtherUnknownEthincityDescription, dOtherUnknownEthincityDescription, "OtherKnownEthnicityDescription");
 			}

 			dBlackAfricanAmerican = (String.valueOf(dBlackAfricanAmerican).replaceFirst("\\.0+$", ""));
 			switch (dBlackAfricanAmerican) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sBlackAfricanAmerican, "sBlackAfricanAmerican");
 			}

 			dWhiteCaucasian = (String.valueOf(dWhiteCaucasian).replaceFirst("\\.0+$", ""));
 			switch (dWhiteCaucasian) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sWhiteCaucasian, "sWhiteCaucasian");
 			}

 			dAsian = (String.valueOf(dAsian).replaceFirst("\\.0+$", ""));
 			switch (dAsian) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sAsian, "sAsian");
 			}

 			dAmericanIndian = (String.valueOf(dAmericanIndian).replaceFirst("\\.0+$", ""));
 			switch (dAmericanIndian) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sAmericanIndian, "sAmericanIndian");
 			}

 			dAlaskanNative = (String.valueOf(dAlaskanNative).replaceFirst("\\.0+$", ""));
 			switch (dAlaskanNative) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sAlaskanNative, "sAlaskanNative");
 			}

 			dNativeHawaiian = (String.valueOf(dNativeHawaiian).replaceFirst("\\.0+$", ""));
 			switch (dNativeHawaiian) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sNativeHawaiian, "sNativeHawaiian");
 			}

 			dOtherPacificIslander = (String.valueOf(dOtherPacificIslander).replaceFirst("\\.0+$", ""));
 			switch (dMexican) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sOtherPacificIslander, "sOtherPacificIslander");
 			}

 			dOtherUnknownRace = (String.valueOf(dOtherUnknownRace).replaceFirst("\\.0+$", ""));
 			switch (dOtherUnknownRace) {
 			case "0": // No
 				break;
 			case "1": // Yes
 				click(sOtherUnknownRace, "sOtherUnknownRace");
 				type(sOtherUnknownRaceDescription, dOtherUnknownRaceDescription, "OtherKnownEthnicityDescription");
 			}

     	} catch(Exception e) {
     		e.printStackTrace();
     	}

    	actionobjects.bSaveAndContinue();
        System.out.println("End of Page or Success - Ethincity and Race Page");
    }

}