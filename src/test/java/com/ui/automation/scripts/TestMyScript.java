package com.ui.automation.scripts;

import java.util.HashMap;

import org.testng.annotations.Test;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;
import com.ui.automation.actions.TestDataPrefix;
import com.ui.automation.pages.HomePage;
import com.ui.automation.utilities.ExcelLib;
import com.ui.automation.utilities.StringUtilities;

/**
 * @author Douglas
 *
 */
public class TestMyScript extends ActionEngine {
	ExcelLib excelLib = new ExcelLib();
	HomePage homepage = new HomePage();
	ActionObjects actionobjects = new ActionObjects();

	@Test(priority = 0)
	public void iServeRoutingLoginAuthentication() throws Throwable {
		try {
			reportLogger = extent.startTest("Verify Site Access and Login", "ISERVE URL and Login");
			HashMap<String,String> dataHomepage = new HashMap<>();
			dataHomepage.put("FirstName", testDataPrefix.get("FirstName"));
			System.out.println(StringUtilities.getRandomString(4));
			String s1 = StringUtilities.generateRandomNumber(10).toUpperCase();
			String s2 = StringUtilities.generateRandomNumberBasedOnLength(7);
			String s3 = StringUtilities.getRandomNumeric(5);
			int s4 = StringUtilities.generateRandomNumber(10, 100);
			homepage.verifyLogin();
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("IserveRoutingLoginAuthentication");
	}

}