package com.ui.automation.scripts;

import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;

import com.ui.automation.actions.ActionEngine;
import com.ui.automation.actions.ActionObjects;
import com.ui.automation.utilities.ExcelLib;
import com.ui.automation.pages.HomePage;
import com.ui.automation.pages.IserveApplicantInfoDOBPage;
import com.ui.automation.pages.IserveApplicantNamePage;
import com.ui.automation.pages.IserveApplicantNameWhereToReachYouPage;
import com.ui.automation.pages.IserveApplicationSubmittedPage;
import com.ui.automation.pages.IserveAuthorizedRepresentativeAddedPage;
import com.ui.automation.pages.IserveAuthorizedRepresentativeAddressPage;
import com.ui.automation.pages.IserveAuthorizedRepresentativeDetailsPage;
import com.ui.automation.pages.IserveAuthorizedRepresentativeOrganizationPage;
import com.ui.automation.pages.IserveAuthorizedRepresentativePage;
import com.ui.automation.pages.IserveAuthorizedRepresentativeScopePage;
import com.ui.automation.pages.IserveBenefitDiscoveryHouseHoldInfoPage;
import com.ui.automation.pages.IserveEthncityRacePage;
import com.ui.automation.pages.IserveFinishingUpPage;
import com.ui.automation.pages.IserveHouseholdAddressPage;
import com.ui.automation.pages.IserveLegalQuestionsPage;
import com.ui.automation.pages.IserveLegalTermsPage;
import com.ui.automation.pages.IserveLegalTermsPage2;
import com.ui.automation.pages.IserveLegalTermsPage3;
import com.ui.automation.pages.IserveLivingSituationPage;
import com.ui.automation.pages.IserveMailDeliveryPage;
import com.ui.automation.pages.IservePersonCompletingApplicationLocationPage;
import com.ui.automation.pages.IservePersonCompletingApplicationPage;
import com.ui.automation.pages.IservePrivacyPage;
import com.ui.automation.pages.IserveProgramSelectPrivacyPage;
import com.ui.automation.pages.IserveProgramSelectionPage;
import com.ui.automation.pages.IserveReviewApplicationPage;
import com.ui.automation.pages.IserveSignaturePage;
import com.ui.automation.pages.IserveVoterRegistrationPage;
import com.ui.automation.pages.IserveApplicantPhonePage;

/**
 * @author Eyasu
 * @author Douglas
 */
public class IserveBeneAppSubmitApplication extends ActionEngine {
	ExcelLib excelLib = new ExcelLib();
	HomePage homepage = new HomePage();
	ActionObjects actionobjects = new ActionObjects();
	IserveProgramSelectionPage IserveProgramSelectionPage = new IserveProgramSelectionPage();
	IserveApplicantNamePage IserveApplicantNamePage = new IserveApplicantNamePage();
	IserveHouseholdAddressPage IserveHouseholdAddressPage = new IserveHouseholdAddressPage();
	IserveLegalQuestionsPage IserveLegalQuestionsPage = new IserveLegalQuestionsPage();
	IserveSignaturePage IserveSignaturePage = new IserveSignaturePage();
	IserveBenefitDiscoveryHouseHoldInfoPage ISERVEHouseHoldPage = new IserveBenefitDiscoveryHouseHoldInfoPage();
	IserveLegalTermsPage IserveLegalTermsPage = new IserveLegalTermsPage(); // Agreements
	IserveLivingSituationPage IserveLivingSituationPage = new IserveLivingSituationPage();
	IserveMailDeliveryPage IserveMailDeliveryPage = new IserveMailDeliveryPage();
	IserveApplicationSubmittedPage IserveApplicationSubmittedPage = new IserveApplicationSubmittedPage();
	IservePrivacyPage IservePrivacyPage = new IservePrivacyPage();
	IserveReviewApplicationPage IserveReviewApplicationPage = new IserveReviewApplicationPage();
	IserveEthncityRacePage IserveEthncityRacePage = new IserveEthncityRacePage();
	IservePersonCompletingApplicationPage IservePersonCompletingApplicationPage = new IservePersonCompletingApplicationPage();
	IservePersonCompletingApplicationLocationPage IservePersonCompletingApplicationLocationPage = new IservePersonCompletingApplicationLocationPage();
	IserveVoterRegistrationPage IserveVoterRegistrationPage = new IserveVoterRegistrationPage();
	IserveProgramSelectPrivacyPage IserveProgramSelectPrivacyPage = new IserveProgramSelectPrivacyPage();
	IserveFinishingUpPage IserveFinishingUpPage = new IserveFinishingUpPage();
	IserveAuthorizedRepresentativeDetailsPage IserveAuthorizedRepresentativeDetailsPage = new IserveAuthorizedRepresentativeDetailsPage();
	IserveAuthorizedRepresentativePage IserveAuthorizedRepresentativePage = new IserveAuthorizedRepresentativePage();
	IserveAuthorizedRepresentativeAddressPage IserveAuthorizedRepresentativeAddressPage = new IserveAuthorizedRepresentativeAddressPage();
	IserveAuthorizedRepresentativeOrganizationPage IserveAuthorizedRepresentativeOrganizationPage = new IserveAuthorizedRepresentativeOrganizationPage();
	IserveApplicantNameWhereToReachYouPage IserveApplicantNameWhereToReachYouPage = new IserveApplicantNameWhereToReachYouPage();
	IserveApplicantInfoDOBPage IserveApplicantInfoDOBPage = new IserveApplicantInfoDOBPage();
	IserveAuthorizedRepresentativeScopePage IserveAuthorizedRepresentativeScopePage = new IserveAuthorizedRepresentativeScopePage();
	IserveLegalTermsPage2 IserveLegalTermsPage2 = new IserveLegalTermsPage2(); // Agreements
	IserveLegalTermsPage3 IserveLegalTermsPage3 = new IserveLegalTermsPage3(); // Agreements
	IserveAuthorizedRepresentativeAddedPage IserveAuthorizedRepresentativeAddedPage = new IserveAuthorizedRepresentativeAddedPage();
	IserveApplicantPhonePage IserveApplicantPhonePage = new IserveApplicantPhonePage();
	
   /**
   * Testing Benefit Application functionality using Excel for Test Data
   *
   * @throws Throwable It will throw exception if any abnormal situation occur while executing the method code.
   */
	@Test(priority = 0)
	public void iServeBeneAppSubmitApplication() throws Throwable {
		try {
			//HashMap<String, String> testData = excelLib.getData("ISERVE", "Framework");
			List<HashMap<String, String>> testDataList = excelLib.getDataList("ISERVE", "SubmitApp");
			//HashMap<String, String> testData = testDataList.get(0);
			for(HashMap<String, String> testData : testDataList) {
			reportLogger = extent.startTest("Verify Applicant can Submit an Application",	"ISERVE Submit Application Excel");
			//homepage.noLoginCredentialsStartBenefitsApplication();
			IserveProgramSelectionPage.setData(testData);
			IserveProgramSelectPrivacyPage.setData(testData);
			IserveApplicantNamePage.setData(testData);
			//IserveApplicantInfoDOBPage.setData(testData);
			IserveLivingSituationPage.setData(testData);
			IserveHouseholdAddressPage.setData(testData);
			IserveReviewApplicationPage.setData(testData);
			IserveApplicantPhonePage.setData(testData);
			IserveApplicantNameWhereToReachYouPage.setData(testData);
			IserveAuthorizedRepresentativePage.setData(testData);
			IserveAuthorizedRepresentativeDetailsPage.setData(testData);
			IserveAuthorizedRepresentativeOrganizationPage.setData(testData);
			IserveAuthorizedRepresentativeAddressPage.setData(testData);
			IserveAuthorizedRepresentativeScopePage.setData(testData);
			IserveAuthorizedRepresentativeAddedPage.setData(testData);
			IserveFinishingUpPage.setData(testData);
			IserveVoterRegistrationPage.setData(testData);
			IserveEthncityRacePage.setData(testData);
			IserveLegalQuestionsPage.setData(testData);
			IserveLegalTermsPage.setData(testData);
			//if programs then {
			//	IserveLegalTermsPage2.setData(testData);
			//	IserveLegalTermsPage3.setData(testData);
			//}
			IservePersonCompletingApplicationPage.setData(testData);
			IservePersonCompletingApplicationLocationPage.setData(testData);
			IserveSignaturePage.setData(testData);
			IserveApplicationSubmittedPage.setData(testData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("IserveBeneAppSubmitApplication");
	}

}